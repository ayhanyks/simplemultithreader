#include "simpleMultiThread.h"
#include "stdio.h"
#include <time.h>

simpleThread myThreads[MAXTHREADS];
simpleThread*currentThread;
unsigned int currentThrnum=0;


void simpleThreadInit(){

    for(int i=0; i<MAXTHREADS;i++)
    {
        myThreads[i].threadStatus=0;    //not initialized..
    }

    currentThread=0;

}

void threadSleepms(unsigned int ms){

    currentThread->wakeupTime=clock() + ms*1000;
    threadRunner();

}
    
int createThread(void*threadFunc, void*inputs, void*output){

    int thrnum=0;

    for(thrnum=0; thrnum<MAXTHREADS;thrnum++)
        if( myThreads[thrnum].threadStatus==0)
            break;


    if(thrnum==MAXTHREADS)
        return -1;
    
    printf("create thread number:%d\n",thrnum);
    myThreads[thrnum].threadStatus=1;
    myThreads[thrnum].mystackCounter=0;
    myThreads[thrnum].entryPoint=threadFunc;
    myThreads[thrnum].input=inputs;
    myThreads[thrnum].output=output;    


    
    
}


unsigned int spval;
register void *sp asm ("sp");
unsigned int mystack0, mystack1;
clock_t curTime;
unsigned int thrnum=0;
void threadRunner()
{

  
   
   //  printf("__________________________________________\n");
    
   //  printf(" f startstack is at 0x%08x\n",sp);
    //find a thread not running

    //1. are we back from a running thread?
   // printf("cthri:%d\n",currentThrnum);
    if(currentThread==0)
    {
        //call the first available thread
           
        for(thrnum=0; thrnum<MAXTHREADS;thrnum++)
            if( myThreads[thrnum].threadStatus==1)
                break;

        currentThread=&(myThreads[thrnum]);
        currentThrnum=thrnum;

        printf("starting thread %d\n",thrnum);
  //      printf("currentThread=%p\n",currentThread->entryPoint );
        mystack0=sp;
  //      printf("stack is at 0x%08x currentThread is 0x%08x \n",sp,currentThread);
        ((void(*)(void))currentThread->entryPoint)();
      //  currentThread->entryPoint();
    }

   //if we are here, then there is a func called us.
   
    //move the stack..
  //  printf("stack is at 0x%08x currentThread is 0x%08x \n",sp,currentThread);
  //  printf("stack depth:%d\n",((unsigned int)mystack0 - (unsigned int)sp)/4);
    currentThread->mystackCounter=0;


    asm("mov %ebp,%esp");
    asm("pop %ebp");

    while((unsigned int)sp<(unsigned int)mystack0){
            asm("pop spval");
            currentThread->mystack[currentThread->mystackCounter++]=spval;
            //thrdAStack[thrdAStackCounter++]=spval;
          //  printf("%p : 0x%08x\n", sp,spval);
    }

   // printf("saved stack:%d\n",currentThread->mystackCounter);
   // printf("stack depth:%d\n",(unsigned int)mystack0 - (unsigned int)sp);
   // printf("stack is back at 0x%08x\n",sp);
    //call the next function that has not started yet or wait time is over..
   thrnum=currentThrnum;
    while(1)
    {
    //   printf("%d->%d [%d]\n",thrnum,myThreads[thrnum].threadStatus,myThreads[thrnum].mystackCounter);
       thrnum++;
       thrnum=(thrnum<MAXTHREADS)?thrnum:0;

       if( myThreads[thrnum].threadStatus==1)
       {   // printf("%d\n",thrnum);
#if 1            
            if(!myThreads[thrnum].mystackCounter)
            {
                //just start it!
                currentThread=&(myThreads[thrnum]);
                currentThrnum=thrnum;
                mystack0=sp;
                printf("starting threadX %d\n",thrnum);
         //       while(1);
                ((void(*)(void))currentThread->entryPoint)();
            }
            else
#endif
           {  

              // printf("%d\n",thrnum);
               curTime=clock();
              // printf("%d\n",thrnum);
              // printf("%lu && %d\n",curTime, thrnum);
               
                if(curTime>myThreads[thrnum].wakeupTime)
                {
                  //  printf("%d --> %d\n", currentThrnum, thrnum);

                    currentThread=&(myThreads[thrnum]);
                    currentThrnum=thrnum;
                    mystack0=sp;
                    //printf("reload stacknum:%d for thread %d\n",currentThread->mystackCounter, currentThrnum);

                    while(currentThread->mystackCounter--){
                            spval=currentThread->mystack[currentThread->mystackCounter];
                     //        printf("0x%08x<---0x%08x\n",sp,spval);
                            asm("push spval"); 
                    }

              //      printf("stack is back at 0x%08x\n",sp);
              //      printf("waking thread no %d\n",currentThrnum);
                   
                  //   printf("stack is at 0x%08x \n",sp);

                   
                    asm("push %ebp");
                    asm("mov %esp,%ebp");
                    return;
                }
                

        
            }

           
            

        }
         
    }
    //if we are here; then no function was run
    
    



    //1. pick the first thread
    //then switch to second available..
    //then third..
   

    

    //lets call function A
    
//    printf("%p\n", sp);
//    printf("XX:thrdBStackCounter:%d\n", thrdBStackCounter);    
#if 0
    if(currentThread==0)
    {   

        //call thread A
        currentThread=(void*)thrdA;
        mystack0=sp;
        thrdA();  

    }
    else if(currentThread==(void*)thrdA)
    {
       
        mystack1=sp;

 //       printf("%p to %p\n", sp,mystack0);

        thrdAStackCounter=0;
        while((unsigned int)sp<(unsigned int)mystack0){
            asm("pop spval");
            thrdAStack[thrdAStackCounter++]=spval;
            //printf("%p : 0x%08x\n", sp,spval);
        }
//        printf("A0:%p\n", sp);

        if(thrdBStatus==0)
        {
            currentThread=(void*)thrdB;
            mystack0=sp;
            thrdBStatus=1;
            thrdB();  
  
        }
        else
        {
           
            mystack0=sp;
//             printf("thrdBStackCounter:%d\n", thrdBStackCounter);    
            while(thrdBStackCounter--){
                spval=thrdBStack[thrdBStackCounter];
                asm("push spval");
                //printf("%p : 0x%08x\n", sp,spval);
            }
//            printf("A1:%p\n", sp);
             currentThread=(void*)thrdB;
            return; 

        }
            
        
        
         

    }
    else if(currentThread==(void*)thrdB){
       
        mystack1=sp;
//          printf("B0:%p\n", sp);
//        printf("%p to %p\n", sp,mystack0);
        thrdBStackCounter=0;
        while((unsigned int)sp<(unsigned int)mystack0){

            asm("pop spval");
            thrdBStack[thrdBStackCounter++]=spval;

           // printf("%p : 0x%08x\n", sp,spval);

        }
//         printf("BB:thrdBStackCounter:%d\n", thrdBStackCounter);
//        printf("B1:%p\n", sp);
           
        currentThread=(void*)thrdA;
       
        mystack0=sp;
        while(thrdAStackCounter--){
            spval=thrdAStack[thrdAStackCounter];
            asm("push spval");
           // printf("%p : 0x%08x\n", sp,spval);
        }
//        printf("B2:%p\n", sp);
//         printf("BB:thrdBStackCounter:%d\n", thrdBStackCounter);
        return; 

       
    }
#endif


}


 
 
