#include <stdio.h>
#include "simpleMultiThread.h"


unsigned int spval;

unsigned int thrdBStackCounter;
unsigned int thrdBStatus=0;

unsigned int thrdAStack[200];
unsigned int thrdAStackCounter;
unsigned int thrdAStatus=0;

unsigned int thrdBStack[200];
void*currentThread=0;


void func1(char X){

    printf("func1 called from thread %c\n",X);
    threadSleepms(10);
   // printf("func1 ret from sleep\n");

}

void thrdA(){
    
    int x=0;
    static int y=0;
    
    while(1){

        printf("in thrd A x=%d y=%d\n", x,y);
        x++;
        y++;
        threadSleepms(10);
       // printf("A ret from sleep\n");
        func1('A');
       // printf("A ret from func1\n");
    }
    
}

void thrdB(){
    
    int x=0;
    static int y=0;
    
    while(1){

        printf("in thrd B x=%d y=%d\n", x,y);
        x++;
        y++;
        threadSleepms(20);
      //  printf("B ret from sleep\n");
         func1('B');
      //  printf("B ret from func1\n");
    }
    
}

void thrdC(){
    
    int x=0;
    static int y=0;
    
    while(1){

        printf("in thrd C x=%d y=%d\n", x,y);
        x++;
        y++;
        threadSleepms(30);
        //func1('C');
       //// printf("ret from sleep\n");

    }
    
}


int main()
{
    printf("Hello World!\n");
    printf("thrdA address:%p\n",thrdA);
    simpleThreadInit();

   // createThread(thrdA,0,0);
   // createThread(thrdB,0,0);
   // createThread(thrdC,0,0);
    createThread(thrdC,0,0);
    createThread(thrdA,0,0);
    createThread(thrdC,0,0);
     createThread(thrdB,0,0);
    createThread(thrdC,0,0);
     createThread(thrdA,0,0);
    createThread(thrdC,0,0);
     createThread(thrdB,0,0);
    createThread(thrdC,0,0);
    createThread(thrdC,0,0);

    threadRunner();
    return 0;

}
