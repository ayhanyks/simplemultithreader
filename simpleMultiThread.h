#ifndef SIMPLEMULTIT_
#define SIMPLEMULTIT_

#define MAXSTACKSIZE    256     //todo: create in heap or sth more clever
#define MAXTHREADS   10

typedef int func(void);


typedef struct {

    unsigned int threadStatus;
    unsigned int mystack[MAXSTACKSIZE];
    unsigned int mystackCounter;
    unsigned int wakeupTime;    
    void*entryPoint;
    void*input;
    void*output;
    
}simpleThread;

void simpleThreadInit();
void threadSleepms(unsigned int ms);
int createThread(void*threadFunc, void*inputs, void*output);
void threadRunner();

#endif





